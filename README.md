###### Assumptions:

1. all files are storing dummy data of specified size.
2. size input from cli is final size of each file
3. block size is of 1KB
4. total memory size = 100KB
5. data can be stored in more than one allocated blocks
6. file with the same name is already created under the path would be replaced
7. folder must exist before creating new file in the location
8. deleting a folder will delete all sub-folders and files



###### Runtime Cli commands examples:
NOTE: 
1. Commands validation has not been implemented and is expected to be entered in following format.
2. extra space and enter are not accepted input. 

create new file: createFile {filePath} {fileSizeInKB}
                --> createFile root/file.txt 100

create new folder: createFolder {folderPath}
                --> createFolder root/newFolder
                
delete folder: deleteFolder {folderPath}
                --> deleteFolder root/newFolder

delete file: deleteFile {filePath}
                --> deleteFile root/file.txt

rename file: renameFile {filePath} {newFileName}
                --> createFolder root/file.txt file2.txt
                
Check Allocated Memory: status
                --> status

check Current VFS: structure
                --> structure