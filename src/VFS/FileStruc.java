package VFS;

import java.util.ArrayList;

public class FileStruc {
    String name;
    ArrayList<Integer> allocatedBlocks;
    public FileStruc() {
        super();
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public ArrayList<Integer> getAllocatedBlocks() {
        return allocatedBlocks;
    }
    public void setAllocatedBlocks(ArrayList<Integer> allocatedBlocks) {
        this.allocatedBlocks = allocatedBlocks;
    }
    boolean deleted;
    public FileStruc(String name, ArrayList<Integer> allocatedBlocks) {
        this.name = name;
        this.allocatedBlocks = allocatedBlocks;
        deleted=false;
    }
}
