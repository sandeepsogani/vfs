package VFS;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        fileOperations t = new MemAllocationService();
        int sizeKB = 100;
        system sys = new system(sizeKB, t);
        Main(sys);
    }

    public static void Main(system sys) {
        String command = "";
        while (!command.equals("exit")) {
            command = new Scanner(System.in).nextLine();
            String str[] = command.trim().split(" ");
            switch (str[0]) {
                case "createFile":
                    sys.createFile(str[1], Integer.parseInt(str[2]));
                    break;
                case "createFolder":
                    sys.createFolder(str[1]);
                    break;
                case "deleteFolder":
                    sys.deleteFolder(str[1]);
                    break;
                case "deleteFile":
                    sys.deleteFile(str[1]);
                    break;
                case "renameFile":
                    sys.renameFile(str[1], str[2]);
                    break;
                case "status":
                    sys.DisplayDiskStatus();
                    break;
                case "structure":
                    sys.DisplayDiskStructure();
                    break;
                case "exit":
                    break;
                default:
                    System.out.println("->No match command");
            }
        }
    }
}
