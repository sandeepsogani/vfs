package VFS;

import java.util.ArrayList;

public class system {
    int sizeKB, allspace = 0;
    fileOperations operation;
    Directory root;
    ArrayList<Boolean> state;
    public int getAllspace() {
        return allspace;
    }

    public void setAllspace(int allspace) {
        this.allspace = allspace;
    }

    public Directory getRoot() {
        return root;
    }

    public void setRoot(Directory root) {
        this.root = root;
    }

    public ArrayList<Boolean> getState() {
        return state;
    }

    public void setState(ArrayList<Boolean> state) {
        this.state = state;
    }

    public ArrayList<Space> getSpaces() {
        return spaces;
    }

    public void setSpaces(ArrayList<Space> spaces) {
        this.spaces = spaces;
    }

    ArrayList<Space> spaces;

    public system(int sizeKB, fileOperations operation) {
        this.sizeKB = sizeKB;
        this.operation = operation;
        state = new ArrayList<>(sizeKB);
        for (int i = 0; i < sizeKB; i++) {
            state.add(false);
        }
        root = new Directory("root");
        spaces = new ArrayList<>();
        spaces.add(new Space(0, sizeKB - 1, false));
    }

    public void createFile(String path, int sizeKB) {
        if (sizeKB > this.sizeKB - this.allspace){
            System.out.println("Unable to allocate memory");
            return;
        }
        String[] paths = path.trim().split("\\\\");
        Directory iter;
        iter = getDire(root, paths, 0);
        if (iter != null) {
            deleteFile(path);
            if (operation.creatFile(iter, paths[paths.length - 1], sizeKB, spaces,
                    state)) {
                this.allspace += sizeKB;
                System.out.println("->file has been created");
                return;
            } else
                System.out.println("Unable to allocate memory");
                return;
        } else
            System.out.println("Unable to allocate memory");
            return;
    }

    public boolean createFolder(String path) {
        String[] paths = path.trim().split("\\\\");
        Directory iter;
        iter = getDire(root, paths, 0);
        if (iter != null)
            return operation.createDir(iter, paths[paths.length - 1]);
        else
            return false;
    }

    public boolean deleteFile(String path) {
        String[] paths = path.trim().split("\\\\");
        Directory iter;
        iter = getDire(root, paths, 0);
        if (iter != null) {
            int fileSize = operation.deleteFile(iter, paths[paths.length - 1],
                    spaces, state);
            if (fileSize != 0) {
                this.allspace -= fileSize;
                System.out.println("file has been deleted");
                return true;
            }
            System.out.println("File does not exists");
            return false;
        } else
            System.out.println("File does not exists");
            return false;
    }

    public boolean renameFile(String path, String newName) {
        String[] paths = path.trim().split("\\\\");
        Directory iter;
        iter = getDire(root, paths, 0);
        if (iter != null) {
            int fileSize = operation.renameFile(iter, paths[paths.length - 1],newName);
            return true;
        } else
            return false;
    }

    public boolean deleteFolder(String path) {
        String[] paths = path.trim().split("\\\\");
        Directory iter;
        iter = getDire(root, paths, 0);
        if (iter != null) {
            int filesSize = operation.deleteDir(iter, spaces, state);
            if (filesSize != 0) {
                this.allspace -= filesSize;
                System.out.println("->folder has been deleted");
                System.out.println("Freed up : " + filesSize);
                return true;
            }
            return false;
        } else
            return false;
    }

    public Directory getDire(Directory dir, String[] path, int level) {
        if(path.length==1)
            return dir;
        for (Directory temp : dir.subDirectory) {
            if (path[level+1].equals(temp.name) && level != path.length - 2) {
                return getDire(temp, path, level + 1);
            }
        }
        if (path[level].equals(dir.name) && level == path.length - 2) {
            return dir;
        }
        return null;
    }

    public void DisplayDiskStructure() {
        root.printDirectoryStructure(0);
    }

    public void DisplayDiskStatus() {
        System.out.println("Empty space:\n" + (sizeKB - allspace) + " KB");
        System.out.println("Allocated space:\n" + allspace + " KB");
        System.out.println("Empty Blocks in the Disk:");
        for (int i = 0; i < state.size(); i++) {
            System.out.println("Block:[" + i + "] is "
                    + (state.get(i) ? "Allocated" : "Empty"));
        }
    }

}
