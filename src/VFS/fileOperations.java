package VFS;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public interface fileOperations {
    boolean creatFile(Directory dir, String name, int sizeKB, ArrayList<Space> spaces, ArrayList<Boolean> state);
    boolean createDir(Directory dir,String name);
    int deleteFile(Directory dir,String name,ArrayList<Space>spaces,ArrayList<Boolean> state);
    int deleteDir(Directory dir,ArrayList<Space>spaces,ArrayList<Boolean> state);
    void write(system sys,String filePath);
    void readTree(system sys, ObjectInputStream os, int currentSize,
                         int sizeKB) throws ClassNotFoundException, IOException;
    int renameFile(Directory dir, String name, String newName);

}
